/* tslint:disable */
/* TODO: Enable TSLint */
import {
    Abstract2dArt,
    AbstractArt,
    AsyncContentComponent,
    declareModule,
    ISystems,
    makeArtModule,
    promptDialogue,
} from '@collboard/modules-sdk';
import * as React from 'react';
import styled from 'styled-components';
import { forAnimationFrame } from 'waitasecond';
import { IVector, Vector } from 'xyzt';
import { CONNECTION_DOT_SNAPRADIUS, DEFAULT_PLOT_BOUNDINGBOX } from '../config';
import { neuralNetworkBuilderDefinitions } from '../definitions/neuralNetworkBuilderDefinitions';
import {
    FunctionBuilderDefinition,
    isNetworkBuilderInputMock,
    isNetworkBuilderLayer, isNetworkBuilderOutputGraph, NetworkBuilderBlock,
    NetworkBuilderInputMock,
    NetworkBuilderLayer,
} from '../interfaces/NeuralNetworkBuilderFunction';
import { functionBuilderFormatTitle } from '../utils/functionBuilderFormatTitle';
import { GraphStateHolder } from '../utils/GraphStateHolder';
import { plot } from '../utils/plot';
import { renderPath } from '../utils/renderPath';
import * as tf from '@tensorflow/tfjs';

const StyledArt = styled.div`
  position: relative;
  border-radius: 10px;

  /* Graph */

  .graphWrapper {
    position: absolute;
    bottom: 7px;
    left: 7px;
    right: 7px;
    height: 180px;
    background: white;
    border-radius: 5px;
    padding: 7px;
  }

  .graphWrapper .graph {
    background: white;
  }

  /* Title */

  .functionTitle {
    width: 230px;
    text-align: center;
    font-size: 1.8em;
    font-weight: bold;
    height: 90px;
    vertical-align: middle;
    display: table-cell;

    .functionSubtitle {
      font-size: 0.6em;
    }
  }

  .functionTitle input {
    width: 110px;
    text-align: center;
    font-size: 1.2em;
    font-weight: bold;
    height: 90px;
    vertical-align: middle;
    display: table-cell;
  }

  /* I/O */

  .inputs,
  .outputs {
    position: absolute;
    top: 10px;
    display: flex;
    flex-direction: column;
    height: 70px;
    z-index: 1;
  }

  .inputs {
    left: -10px;
  }

  .outputs {
    right: -10px;
  }

  .inputs .connection,
  .outputs .connection {
    display: flex;
    margin: auto 0;
  }

  .inputs .connection {
    flex-direction: row;
  }

  .outputs .connection {
    flex-direction: row-reverse;
  }

  .connectionPoint {
    display: inline-block;
    width: 24px;
    height: 24px;
    background: #f2f2f2;
    border: solid 4px white;
    border-radius: 100%;
    pointer-events: all;
  }

  .functionBuilder-tool & .connectionPoint:hover {
    border: solid 3px white;
    cursor: pointer;
  }

  .connectionTitle {
    margin: 0 8px;
    line-height: 24px;
    font-size: 0.9em;
  }
`;

export class NeuralNetworkBuilderArt extends Abstract2dArt {
    // __Underscored variables don't get serialized
    public __pointerOverOutput: boolean = false;
    public __pointerOverInput: { [key: string]: boolean } = {};

    private __outputRef = React.createRef<HTMLDivElement>();
    private __inputRefs: { [key: string]: React.RefObject<HTMLDivElement> } = {};

    private __lastPlotted: number = -1;

    public connections: { [key: string]: string | null };
    private privateSize: IVector = new Vector(230, 280);
    public color: string;

    public userConfig: { [key: string]: any } = {};
    private __originalUserConfig: { [key: string]: any } = {};
    private layerWeights: any; // TODO Saving network weights
    private __tempLayer: any;

    constructor(public shift: IVector, private funct: string) {
        super();

        this.color = GraphStateHolder.generateColor();
        this.connections = {};

        // Set defaultValue to userConfig where it is not defined
        Object.entries(this.blockDefinition?.userConfig || {}).map(([key, value]) => {
            if (this.userConfig[key] === undefined) this.userConfig[key] = value.defaultValue;
        });

        // Create block inputs
        Object.keys(this.blockDefinition?.blockInputs || {}).forEach(inputKey => {
            this.__pointerOverInput[inputKey] = false;
            this.connections[inputKey] = null;
            this.__inputRefs[inputKey] = React.createRef();
        });

        if (isNetworkBuilderInputMock(this.blockDefinition)) this.privateSize.y = 140;
    }

    acceptedAttributes = [];

    // The size of BB is reduced because of the connection events (should be eventually fixed)
    get topLeftCorner() {
        return Vector.add(this.shift, Vector.box(10), new Vector(0, 75));
    }

    get bottomRightCorner() {
        return Vector.subtract(Vector.add(this.shift, this.privateSize), Vector.box(10));
    }

    public get blockDefinition(): FunctionBuilderDefinition | null {
        return neuralNetworkBuilderDefinitions[this.funct];
    }

    public registerInputIfOver(sourceId: string, position: Vector) {
        this.recountPointerOverInputByPosition(position);
        Object.keys(this.__pointerOverInput)
            .filter((key) => this.__pointerOverInput[key])
            .forEach((key) => {
                this.connections[key] = sourceId;
                GraphStateHolder.update();
                this.__pointerOverInput[key] = false;
                //console.log('Created connection from ' + sourceId + ' to ' + this.artId + ' (input ' + key + ')');
            });
    }

    private recountPointerOverInputByPosition(position: Vector) {
        Object.entries(this.__inputRefs)
            .map(([key, ref]) => ({ key, current: ref.current }))
            .filter(({ current }) => current)
            .map(({ key, current }) => {
                const { x, y, width, height } = current!.getBoundingClientRect();
                const refPosition = new Vector(x + width / 2, y + height / 2);
                return {
                    key,
                    current,
                    distance: Vector.distance(refPosition, position),
                };
            })
            .filter(({ distance }) => distance < CONNECTION_DOT_SNAPRADIUS)
            .sort(({ distance: a }, { distance: b }) => (a > b ? 1 : -1))
            .slice(0, 1)
            .forEach(({ key }) => {
                this.clearAllPointerOverInputs();
                this.__pointerOverInput[key] = true;
            });
    }

    private clearAllPointerOverInputs() {
        for (const key in this.__pointerOverInput) {
            this.__pointerOverInput[key] = false;
        }
    }

    private locateRef(target: React.RefObject<HTMLDivElement>, systemsContainer: ISystems) {
        if (target && target.current) {
            const bb = target.current.getBoundingClientRect();
            return systemsContainer.collSpace.pickPoint(new Vector(bb.x, bb.y)).point.add(Vector.box(12)); // 12 is radius of circle
        }
        return Vector.add(this.shift, Vector.scale(this.privateSize, 0.5));
    }

    public getOutputPosition(systemsContainer: ISystems) {
        return this.locateRef(this.__outputRef, systemsContainer);
    }

    public getInputPosition(key: string, systemsContainer: ISystems) {
        return this.locateRef(this.__inputRefs[key], systemsContainer);
    }

    // TODO evaluate()
    public evaluate(originalInputs: any[] | null, seenNodes: string[], systemsContainer: ISystems): ({ firstInputs: any[] | null; layers: { outSize?: number[], layer: any }[] } | null) {
        console.log('EVAL1');
        if (seenNodes.includes(this.artId)) return null;
        if (!this.blockDefinition) return null;
        console.log('EVAL2');

        // Get proceeding blocks
        let sources: { [key: string]: NeuralNetworkBuilderArt | null } = {};
        Object.keys(this.connections).forEach((key) => {
            if (this.connections[key] === null) {
                sources[key] = null;
                return;
            }

            const foundArts = systemsContainer.materialArtVersioningSystem.arts.filter(
                (art: AbstractArt) => art.artId === this.connections[key],
            );
            if (foundArts.length === 0) {
                sources[key] = null;
                return;
            }

            sources[key] = foundArts[0] as NeuralNetworkBuilderArt;
        });
        console.log('sources: ', sources);

        // Evaluate input blocks
        let inputs: { [key: string]: ReturnType<(typeof NeuralNetworkBuilderArt.prototype.evaluate)> } = {};
        Object.keys(sources).forEach((key) => {
            if (sources[key] === null) {
                inputs[key] = null;
                return;
            }

            inputs[key] = sources[key]!.evaluate(originalInputs, [...seenNodes, this.artId], systemsContainer);
        });
        console.log('inputs: ', inputs);

        // Check if all inputs exist
        if (Object.values(inputs).reduce((prev, curr) => prev || curr === null, false)) return null;

        // Process block InputMock
        if (isNetworkBuilderInputMock(this.blockDefinition)) {
            if (this.userConfig != this.__originalUserConfig) {
                this.__tempLayer = tf.input({ shape: this.userConfig.dimensions });
                this.__originalUserConfig = this.userConfig;
            }
            return {
                firstInputs: originalInputs,
                layers: [{ layer: this.__tempLayer, outSize: this.userConfig.dimensions }],  // TODO - outSize
            };
        }
        // Process block Layer
        if (isNetworkBuilderLayer(this.blockDefinition)) {
            if (this.userConfig != this.__originalUserConfig) {
                this.__tempLayer = this.blockDefinition.buildLayer(this.userConfig);
                this.__originalUserConfig = this.userConfig;
            }
            return {
                firstInputs: originalInputs,
                layers: [...(inputs?.['networkInput']?.layers || []), { layer: this.__tempLayer }],  // TODO - outSize
            };
        }
        // Process block OutputGraph
        if (isNetworkBuilderOutputGraph(this.blockDefinition)) {
            if (this.userConfig != this.__originalUserConfig) {
                // this.__tempLayer = this.blockDefinition.buildLayer(this.userConfig);
                this.__originalUserConfig = this.userConfig;
            }
            return {
                firstInputs: originalInputs,
                layers: inputs?.['networkInput']?.layers || [],  // TODO - outSize
            };
        }

        // TODO Return network things
        return null;
    }

    render(_selected: boolean, systemsContainer: ISystems) {
        console.log('RENDER');
        if (Object.keys(this.connections).length !== Object.keys(this.__inputRefs).length) {
            Object.keys(this.blockDefinition?.blockInputs || {}).forEach(inputKey => {
                this.__inputRefs[inputKey] = React.createRef();
            });
        }

        let sources: { [key: string]: NeuralNetworkBuilderArt | null } = {};
        Object.keys(this.connections).forEach((key) => {
            if (this.connections[key] === null) {
                sources[key] = null;
                return;
            }

            const foundArts = systemsContainer.materialArtVersioningSystem.arts.filter(
                (art: AbstractArt) => art.artId === this.connections[key],
            );
            if (foundArts.length === 0) {
                // Object got deleted
                sources[key] = null;
                this.connections[key] = null;
                GraphStateHolder.update();
                return;
            }

            sources[key] = foundArts[0] as NeuralNetworkBuilderArt;
        });

        let outputSize: string = '';
        const ev = this.evaluate(null, [], systemsContainer);

        let graphFunc: (x: number) => number = (x) => 0;
        if (isNetworkBuilderOutputGraph(this.blockDefinition)) {
            console.log('XXX - out-ev: ', ev);
            if (ev) {
                try {
                    const model = tf.sequential();
                    console.log('Layers: ', ev.layers.map(lay => lay.layer));
                    const inputShape = ev.layers[0].outSize || [];
                    if (!inputShape) {
                        console.log('No input to network.');
                    } else {
                        console.log('Input shape: ', inputShape);

                        ev.layers.slice(1).forEach((lay, index) => {
                            console.log('Adding: ', lay.layer);
                            if (index == 0) {
                                lay.layer.batchInputShape = [1, ...inputShape];  // Set input shape for layer.
                            }
                            model.add(lay.layer);
                        });
                        console.log("Model completed");
                        graphFunc = (x: number) => {
                            console.log("graphFunc");
                            try {
                                console.log('XXX - PREDICTING');
                                const pred = model.predict(tf.tensor1d([x]));
                                console.log('XXX - pred: ', pred);
                                // @ts-ignore
                                return pred.arraySync()[0];
                            } catch (e) {
                                console.error('Cant do the graph! Error: ', e);
                                return 0;
                            }
                        };
                    }
                } catch (e) {
                    console.error('Cant build network! Error: ', e);
                }
            } else {
                console.log('XXX - ev is null!');
            }
        }

        return (
            <StyledArt
                className='block'
                style={{
                    width: this.privateSize.x || 0,
                    height: this.privateSize.y || 0,
                    position: 'absolute',
                    left: this.shift.x || 0,
                    top: this.shift.y || 0,
                    transform: 'rotate(' + this.rotation + 'deg)',
                }}
            >
                {/* HEADER */}
                <div className='functionTitle'>
                    {!this.blockDefinition ? (
                        'Error'
                    ) : (
                        <>
                            <div>{this.blockDefinition.title}</div>
                            {Object.entries(this.blockDefinition.userConfig || {}).map(([key, conf]) =>
                                <div key={key} className='functionSubtitle' onClick={async (event) => {
                                    // TODO: I just do not figure out how to do it with input so I am using this quickhack with prompt
                                    event.stopPropagation();

                                    const value = await promptDialogue({
                                        defaultValue: conf.createString(this.userConfig[key]),
                                    });
                                    if (!value) return;

                                    try {
                                        const parsedValue = conf.fromString(value);
                                        console.log('Parsed: ', parsedValue);

                                        // TODO: Creating opertions in arts (art is changing itself) should have nicer API
                                        systemsContainer.materialArtVersioningSystem
                                            .createOperation('Change value of constant')
                                            .takeArts(this)
                                            .updateWithMutatingCallback((art) => {
                                                art.userConfig[key] = parsedValue;
                                            })
                                            .persist();

                                        GraphStateHolder.update();
                                    } catch (e) {
                                        console.warn('Can\'t parse, exception: ', e);
                                        alert('Parsing error!');
                                        return;
                                    }
                                }}>
                                    {isNetworkBuilderInputMock(this.blockDefinition) ? <>
                                        {conf.title}:&nbsp;
                                        {conf.prettyString(this.userConfig[key])} ✏️</> : null}
                                </div>,
                            )}
                        </>
                    )}
                </div>

                {/* INPUTS */}
                <div className='inputs'>
                    {Object.keys(this.blockDefinition?.blockInputs || {}).map((key) => (
                        <div className='connection' key={key}>
                            <div
                                className='connectionPoint'
                                onPointerOver={() => {
                                    this.__pointerOverInput[key] = true;
                                }}
                                onPointerOut={() => {
                                    this.__pointerOverInput[key] = false;
                                }}
                                style={sources[key] ? { background: sources[key]!.color } : {}}
                                ref={this.__inputRefs[key]}
                            />
                            <div className='connectionTitle'>
                                <i>
                                    {/* TODO - Input show input dimensions */}
                                </i>
                            </div>
                        </div>
                    ))}
                </div>

                {/* OUTPUTS */}
                <div className='outputs'>
                    {(isNetworkBuilderInputMock(this.blockDefinition) || isNetworkBuilderLayer(this.blockDefinition)) ?
                        <div className='connection'>
                            <div
                                className='connectionPoint'
                                onPointerOver={() => {
                                    this.__pointerOverOutput = true;
                                }}
                                onPointerOut={() => {
                                    this.__pointerOverOutput = false;
                                }}
                                ref={this.__outputRef}
                                style={{ background: this.color }}
                            />
                            <div className='connectionTitle'>
                                {outputSize} {/* TODO Show output dimensions */}
                            </div>
                        </div> : null}
                </div>

                {/* Graph output */}
                {isNetworkBuilderOutputGraph(this.blockDefinition) ?
                    <div className='graphWrapper'>
                        <canvas
                            className='graph'
                            width={202}
                            height={166}
                            id={this.artId}
                            ref={(canvas) => {
                                if (canvas) {
                                    if (!this.blockDefinition) return;
                                    if (GraphStateHolder.lastPlotted === this.__lastPlotted) return;

                                    this.__lastPlotted = GraphStateHolder.lastPlotted;
                                    plot({
                                        canvas,
                                        func: graphFunc,
                                        // func: (x) => Math.sin(x),
                                        boundingBox: DEFAULT_PLOT_BOUNDINGBOX,
                                        // TODO: objects: {},
                                    });
                                }
                            }}
                        />
                    </div> : null}

                {/* Some hacks to make it work I suppose. 🤔 */}
                <AsyncContentComponent
                    content={async () => {
                        // Note: this is tiny hack to get connections in correct positions on initial load
                        // TODO - Still broken xD
                        await forAnimationFrame();
                        return (
                            <>
                                {Object.keys(sources)
                                    .filter((key) => sources[key] !== null)
                                    .map((key) => {
                                        return renderPath(
                                            sources[key]!.getOutputPosition(systemsContainer),
                                            this.getInputPosition(key, systemsContainer),
                                            sources[key]!.color,
                                            undefined, // Here can be label
                                            this.shift,
                                            key,
                                        );
                                    })}
                            </>
                        );
                    }}
                />
            </StyledArt>
        );
    }

}

declareModule(makeArtModule({
        name: 'NeuralNetworkBuilder',
        class: NeuralNetworkBuilderArt,
    },
));
