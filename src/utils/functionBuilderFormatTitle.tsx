import * as React from 'react';
import { FunctionBuilderDefinition } from '../interfaces/NeuralNetworkBuilderFunction';

export function functionBuilderFormatTitle(definition: FunctionBuilderDefinition): JSX.Element {
    return <span>{definition.title}</span>;
}
