import { FunctionBuilderDefinition } from '../interfaces/NeuralNetworkBuilderFunction';
import * as tf from '@tensorflow/tfjs';

class ValidationError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = 'ValidationError';
    }
}

export const neuralNetworkBuilderDefinitions: { [key: string]: FunctionBuilderDefinition } = {
    // region INPUTS
    mockInput: {
        type: 'NETWORK_BUILDER_INPUT_MOCK',
        title: 'Mock Input',
        userConfig: {
            dimensions: {
                title: 'Dimensions',
                fromString: str => {
                    const value = str.split(',').map(v => parseInt(v));
                    value.forEach(val => {
                        if (val <= 0) throw new ValidationError('Dimension <= 0');
                        if (val != Math.round(val)) throw new ValidationError('Dimension not integer');
                    });
                    return value;
                },
                createString: val => val?.join(',') || '',
                prettyString: val => `(${val?.join(',') || ', '})`,
                defaultValue: [64, 64],
            },
        },
    },
    // endregion

    // region LAYERS
    denseLayer: {
        type: 'NETWORK_BUILDER_LAYER',
        title: 'Dense layer',
        userConfig: {
            units: {
                title: 'Units',
                fromString: str => {
                    const val = parseInt(str);
                    if (val <= 0) throw new ValidationError('Dense units <= 0');
                    if (val != Math.round(val)) throw new ValidationError('Dense units not integer');
                    if (val > 65536) throw new ValidationError('Dense units > 65536.');
                    return val;
                },
                createString: val => val?.toString() || 'undefined',
                prettyString: val => val?.toString() || 'doubleclick to set',
                defaultValue: 64,
            },
        },
        blockInputs: { networkInput: {} },
        dimsInput: 1,
        dimsOutput: 1,
        buildLayer: (_args) => tf.layers.dense({ ..._args, activation: 'linear' }),  // Activation will be in separate layer
    },
    // endregion

    // region OUTPUTS
    outputGraph: {
        type: 'NETWORK_BUILDER_OUTPUT_GRAPH',
        title: 'Output graph',
        blockInputs: { networkInput: {} },
    },
    // endregion
};
