export type FunctionBuilderDefinition = NetworkBuilderInputMock | NetworkBuilderLayer | NetworkBuilderOutputGraph;

export interface NetworkBuilderBlock {
    type: string;
    title: string;
    userConfig?: {
        [key: string]: {
            title: string;
            fromString: (value: string) => any;
            createString: (value: any | undefined | null) => string;
            prettyString: (value: any | undefined | null) => string;
            defaultValue: any;
        }
    };
    blockInputs?: { [key: string]: any }
}

//////////////////////////////////////////

export interface NetworkBuilderInputMock extends NetworkBuilderBlock {
    type: 'NETWORK_BUILDER_INPUT_MOCK';
}

export interface NetworkBuilderLayer extends NetworkBuilderBlock {
    type: 'NETWORK_BUILDER_LAYER';
    dimsInput: number | 'any';
    dimsOutput: number | 'any';
    buildLayer: (config: any) => any;
}

export interface NetworkBuilderOutputGraph extends NetworkBuilderBlock {
    type: 'NETWORK_BUILDER_OUTPUT_GRAPH';
}


//////////////////////////////////////////

export function isNetworkBuilderInputMock(
    functionDefinition: FunctionBuilderDefinition | null,
): functionDefinition is NetworkBuilderInputMock {
    return functionDefinition?.type == 'NETWORK_BUILDER_INPUT_MOCK';
}

export function isNetworkBuilderLayer(
    functionDefinition: FunctionBuilderDefinition | null,
): functionDefinition is NetworkBuilderLayer {
    return functionDefinition?.type == 'NETWORK_BUILDER_LAYER';
}

export function isNetworkBuilderOutputGraph(
    functionDefinition: FunctionBuilderDefinition | null,
): functionDefinition is NetworkBuilderOutputGraph {
    return functionDefinition?.type == 'NETWORK_BUILDER_OUTPUT_GRAPH';
}
